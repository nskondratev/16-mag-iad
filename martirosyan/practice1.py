import os
from scipy.io.wavfile import read as wavread
import matplotlib.pyplot as plt  
from numpy.fft import rfft, ifft
import numpy as np
%matplotlib inline

folder = 'numeral/'
filelist = os.listdir(folder) 
X,y=[],[]
for i in filelist:
    for filename in os.listdir(folder + i):
        if filename[-4:] == '.wav':
            y.append(int(i))
            [s, x] = wavread(folder + i + '/' + filename)
            xft= np.abs(rfft(x, 1024))**2
            xft_Db = 10* np.log(xft)
            X.append((np.abs(ifft(xft_Db, 64))**2)[:32])
            
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 7)

from sklearn import neighbors
model = neighbors.KNeighborsClassifier(n_neighbors = 5)
model.fit(X_train, y_train)

err_train = np.mean(y != model.predict(X))
err_test = np.mean(y_test != model.predict(X_test))

print (err_train)
print (err_test)