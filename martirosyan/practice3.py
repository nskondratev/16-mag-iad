from python_speech_features import mfcc
from sklearn.mixture import GaussianMixture
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from fastdtw import fastdtw
import scipy.io.wavfile as wav
import numpy as np
import os
%matplotlib inline

folder = 'numeral/'
filelist = os.listdir(folder) 
X, y = [], []
for i in filelist:
    for filename in os.listdir(folder + i):
        if filename[-4:] == '.wav':
            (rate, sig) = wav.read(folder + i + '/' + filename)
            features =mfcc(sig, rate)
            for feature in features:
                X.append(feature)
                y.append(int(m))

model = GaussianMixture(n_components = 10)
model.fit(X)
            
                
Xx, yy = [], []
for i in filelist:
    file_count = 0
    for filename in os.listdir(folder + i):
        if filename[-4:] == '.wav':
            if file_count < 25:
                (rate, sig) = wav.read(folder + i + '/' + filename)
                Xx.append(model.predict(mfcc(sig, rate)))
                yy.append(int(i))
                file_count += 1
                

X_train, X_test, y_train, y_test = train_test_split(Xx, yy, test_size = 0.33, random_state = 7)

n = len(X_train)
distances = np.zeros((n, n))
for i in range(0, n):
    for j in range(i + 1, n):
        distance, path = fastdtw(X_train[i], X_train[j])
        distances[i][j] = distance
        distances[j][i] = distance

knn = KNeighborsClassifier(metric = "precomputed")
knn.fit(distances, y_train)

n_test = len(X_test)
n_train = len(X_train)
test_distances = np.zeros((n_test, n_train))

for i in range(0, n_test):
    for j in range(0, n_train):
        distance, path = fastdtw(X_test[i], X_train[j])
        test_distances[i][j] = distance
        
y_predict = knn.predict(test_distances)
print(classification_report(y_test, y_predict))
