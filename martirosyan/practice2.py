from python_speech_features import mfcc
from sklearn.mixture import GaussianMixture
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
import scipy.io.wavfile as wav
import numpy as np
import os

folder = 'numeral/'
filelist = os.listdir(folder) 
X, y = [], []
for i in filelist:
    for filename in os.listdir(folder + i):
        if filename[-4:] == '.wav':
            (rate, sig) = wav.read(folder + i + '/' + filename)
            features = mfcc(sig, rate)
            for feature in features:
                X.append(feature)
                y.append(int(i))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 7)
model = GaussianMixture(n_components=10, covariance_type='diag')
model.fit(np.array(X_train, dtype=np.float64))
predictions = model.predict(X_test)

err_test = np.mean(y_test != predictions)

print ("mixture errors:", err_test)

model = neighbors.KNeighborsClassifier(n_neighbors = 5)
model.fit(X_train, y_train)

predictions = model.predict(X_test)


err_test = np.mean(y_test != predictions)

print ("knn errors:", err_test)
