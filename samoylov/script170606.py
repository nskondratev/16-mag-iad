from python_speech_features import mfcc
from sklearn.mixture import GaussianMixture
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
import scipy.io.wavfile as wav
import numpy as np
import os

numeral = 'numeral/'
filelist = os.listdir(numeral) 
X, y = [], []
for m in filelist:
    for filename in os.listdir(numeral + m):
        if filename[-4:] == '.wav':
            (rate, sig) = wav.read(numeral + m + '/' + filename)
            features = mfcc(sig, rate)
            for feature in features:
                X.append(feature)
                y.append(int(m))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 7)
model = GaussianMixture(n_components=10, covariance_type='diag')
model.fit(np.array(X_train, dtype=np.float64))
predictions = model.predict(X_test)

print(classification_report(y_test, predictions))