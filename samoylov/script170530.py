import os
from scipy.io.wavfile import read as wavread
import matplotlib.pyplot as plt  
from numpy.fft import rfft, ifft
import numpy as np
%matplotlib inline
    
def cepstr(filename):
    [s, x] = wavread(filename)
    xft= np.abs(rfft(x, 1024))**2
    xft_Db = 10* np.log(xft)
    return (np.abs(ifft(xft_Db, 64))**2)[:32]
	
def get_errors(model):
	err_train = np.mean(y != model.predict(X))
	err_test = np.mean(y_test != model.predict(X_test))
	return err_train, err_test
	
def get_classification_report(y_test, y_test_pred):
	from sklearn.metrics import classification_report
	return (classification_report(y_test, y_test_pred))

numeral = 'numeral/'
filelist = os.listdir(numeral) 
X,y=[],[]
for m in filelist:
    for filename in os.listdir(numeral + m):
        if filename[-4:] == '.wav':
            y.append(int(m))
            X.append(cepstr(numeral + m + '/' + filename))
			
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 7)

from sklearn.svm import SVC
svc_model = SVC(kernel = 'linear')
svc_model.fit(X_train, y_train)
print(get_errors(svc_model))
print(get_classification_report(y_test, svc_model.predict(X_test)))

from sklearn.linear_model import LogisticRegression
logistic_model = LogisticRegression()
logistic_model.fit(X_train, y_train)
print(get_errors(logistic_model))
print(get_classification_report(y_test, logistic_model.predict(X_test)))

from sklearn import neighbors
k_neighbors_model = neighbors.KNeighborsClassifier(n_neighbors = 1)
k_neighbors_model.fit(X_train, y_train)
print(get_errors(k_neighbors_model))
print(get_classification_report(y_test, k_neighbors_model.predict(X_test)))