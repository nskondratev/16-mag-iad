from python_speech_features import mfcc
from sklearn.mixture import GaussianMixture
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from fastdtw import fastdtw
import scipy.io.wavfile as wav
import numpy as np
import os
%matplotlib inline

def get_mfcc_features(file):
    (rate, sig) = wav.read(file)
    return mfcc(sig, rate)
	
numeral = 'C:/Users/Mikhail/numeral/'
filelist = os.listdir(numeral) 
X, y = [], []
for m in filelist:
    for filename in os.listdir(numeral + m):
        if filename[-4:] == '.wav':
            for feature in get_mfcc_features(numeral + m + '/' + filename):
                X.append(feature)
                y.append(int(m))

Xx, yy = [], []
for m in filelist:
    files_count = 0
    for filename in os.listdir(numeral + m):
        if filename[-4:] == '.wav':
            if files_count < 25:
                Xx.append(model.predict(get_mfcc_features(numeral + m + '/' + filename)))
                yy.append(int(m))
                files_count += 1

model = mixture.GaussianMixture(n_components = 10)
model.fit(X)

X_train, X_test, y_train, y_test = train_test_split(Xx, yy, test_size = 0.33, random_state = 7)

n = len(X_train)
distances = np.zeros((n, n))
for i in range(0, n):
    for j in range(i + 1, n):
        distance, path = fastdtw(X_train[i], X_train[j])
        distances[i][j] = distance
        distances[j][i] = distance

knn = KNeighborsClassifier(metric = "precomputed")
knn.fit(distances, y_train)

n_test = len(X_test)
n_train = len(X_train)
test_distances = np.zeros((n_test, n_train))

for i in range(0, n_test):
    for j in range(0, n_train):
        distance, path = fastdtw(X_test[i], X_train[j])
        test_distances[i][j] = distance
		
y_predict = knn.predict(test_distances)
print(classification_report(y_test, y_predict))
